var actors = [
    {
        name: 'Harrison Ford',
        birthday: '13-7-1942',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTY4Mjg0NjIxOV5BMl5BanBnXkFtZTcwMTM2NTI3MQ@@._V1_UX214_CR0,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'Raiders of the lost ark',
                year: '1981'
            }
        ]
    },{
        name: 'Michelle Pfeiffer',
        birthday: '29-04-1956',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTUzNjI0Njc5NF5BMl5BanBnXkFtZTYwOTM2MjYz._V1_UX214_CR0,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'Dangerous Minds',
                year: '1995'
            },
            {
                name: 'The Fabulous Baker Boys',
                year: '1989'
            }
        ]
    },{
        name: 'Arnold Schwarzenegger',
        birthday: '30-7-1947',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTI3MDc4NzUyMV5BMl5BanBnXkFtZTcwMTQyMTc5MQ@@._V1_UY317_CR19,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'Total Recall',
                year: '1990'
            }
        ]
    },{
        name: 'Meg Ryan',
        birthday: '19-11-1961',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTgzOTI0NzI3OV5BMl5BanBnXkFtZTgwODIyMzUzMDI@._V1_UY317_CR1,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'When Harry Met Sally',
                year: '1989'
            },
            {
                name: 'Sleepless In Seattle',
                year: '1993'
            }
        ]
    }
];

function update() {
    var $container = $('#main-container');
    $container.empty();
    for (var i=0; i<actors.length; i++) {
        var actor = actors[i];

        var $name = $('<div class="name"></div>');//jquery functie, nieuwe div maken(nieuw html elemnt)
        $name.html(actor.name);//=innerhtml, in div word naam vd acteur weergegeven

        var $birthDay = $('<div class="birthday"></div>');//zelfde als erboven
        $birthDay.html(actor.birthday);//hetzelfde als erboven

        var $img = $('<img class="image">');//nieuwe elemnt img
        $img.attr("src",actor.pictures);//atrbiuut (style, class, id, name)src vd image op actors.pictures

        var $imgContainer = $('<div class="image-container"></div>');//maakt weer element div
        $imgContainer.append($img);//append = image in container steken

        var $movieContainer = $('<div class="movie-container"></div>');//weer element div maken, met movie container
        for (var j=0; j<actor.movies.length; j++) { //loopt door alle films vd acteurs
            var movie = actor.movies[j];//welke movie
            var $movie = $('<div class="movie"></div>');//maakt div met class movie in $movie
            $movie.html(movie.name + ' (' + movie.year + ')');//in $movie ga met .html iets anders insteken, met naam vd film + het jaar
            $movieContainer.append($movie);//stopt movie in movie-container
        }

        var $delete = $('<div class="delete">x</div>');//maakt nieuwe element aan
        $delete.data('id',i);//.data = enerwelk object in java-element, steek nr film vd film
        $delete.hide();//nog niet kunt zien
        $delete.click(function() {
            actors.splice($(this).data('id'),1);//nummer terug opvragen
            update();
        });


        var $actorContainer = $('<div class="actor-container"></div>');//elemnt div met actors-container
        $actorContainer.append($name).append($birthDay).append($imgContainer).append($movieContainer).append($delete);//alles ervoor aangemaakt in nieuwe div
        $container.append($actorContainer);//alles in actor-container steken

        $actorContainer.mouseover(function() {//over kaart dan komt deleteknop erop
            $(this).find('.delete').show();//this = elemnt waarover je muis is (jqueryobject van maken), zoek container met delete en showt het
        });
        $actorContainer.mouseout(function() {//van kaart weg deleteknop weg
            $(this).find('.delete').hide();//zoekt container met delete en verstopt het
        });
    }

}
update();